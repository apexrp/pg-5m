import * as fs from 'fs';
import * as path from 'path';

export class Logger {
  output: string;
  file_stream: fs.WriteStream;

  constructor(output: string) {
    this.output = output;
    this.file_stream = null;
    if (this.output == 'file' || this.output == 'both') {
      this.file_stream = fs.createWriteStream(path.resolve(__dirname, 'pg-5m.log'));
    }
  }

  writeConsole(msg: string) {
    console.log(`[PG-5m] ${msg}`)
  }

  writeFile(msg: string) {
    this.file_stream.write('[PG-5m] ${msg}\n')
  }

  log(msg: string) {
    switch (this.output) {
      case 'console':
        this.writeConsole(msg);
        break;

      case 'file':
        this.writeFile(msg);
        break;

      case 'both':
        this.writeConsole(msg);
        this.writeFile(msg);
        break;
    }
  }

  error(msg: string) {
    this.log(`[ERROR] ${msg}`)
  }
  warn(msg: string) {
    this.log(`[WARN] ${msg}`)
  }
  info(msg: string) {
    this.log(`[INFO] ${msg}`)
  }
}
