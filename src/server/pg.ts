import { Pool, QueryConfig } from "pg";
import { Logger } from "./logger";

export class Pg {
  logger: Logger;
  pool: Pool;

  constructor(connection_string: string, logger: Logger) {
    this.logger = logger;
    this.pool = new Pool();
    this.pool = new Pool({
      connectionString: connection_string
    });

    this.pool.query('SELECT NOW()', (err, res) => {
      if (!err) {
        this.logger.log('Database server connection established.')
      } else {
        this.logger.error(`${err.message}`)
      }
    })
  }

  async execute(query: QueryConfig, connection?) {
    var conn = connection || await this.pool.connect();
    try {
      return conn.query(query);
    } catch (err) {
      this.logger.error(`${err.message}`)
    }
    finally {
      if (!connection) {
        conn.release();
      }
    }
  }
}
