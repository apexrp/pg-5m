import { QueryConfig } from "pg";
import { createHash } from "crypto";

export function prepareQuery(query: string, values: string[]): QueryConfig {
    return {
        name: createHash("sha1").update(query).digest("hex"),
        text: query,
        values: values
    }
}

export function safeInvoke(callback, result) {
    if (typeof(callback) === 'function') {
        setImmediate(() => {
            callback(result);
        });
    }
}