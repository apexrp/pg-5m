import { toExports } from "./helper";
import { Pg } from "./pg";
import { QueryResult } from "pg";

import { prepareQuery, safeInvoke } from "./util";
import { Logger } from "./logger";

var logger: Logger = null;
var pg: Pg = null;

var isReady: boolean = false;

toExports('is_ready', () => isReady);

toExports('pg_execute', (sql, parameters, callback) => {
  const invokingResource: string = GetInvokingResource();
  const query = prepareQuery(sql, parameters);
  pg.execute(query).then((result) => {
    safeInvoke(callback, result);
    return true;
  }).catch(() => false);
});


on('onServerResourceStart', (resource_name) => {
  if (resource_name == 'pg-5m') {
    logger = new Logger(GetConvar('pg_debug_output', 'console'));

    const connection_string: string = GetConvar('pg_connection_string', '');
    if (connection_string === '') {
      logger.error('Empty pg_connection_string.');
    }
    else {
      pg = new Pg(connection_string, logger);
      emit('onPgReady');
      isReady = true;
    }
  }
})
