const webpack = require("webpack");
const path = require("path");

module.exports = {
    entry: path.resolve(__dirname, 'src', 'server', 'index.ts'),
    mode: 'production',
    target: "node",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    optimization: {
        minimize: false,
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        new webpack.IgnorePlugin(/^pg-native$/)
      ],
    output: {
        filename: 'server.js',
        path: path.resolve(__dirname, 'dist'),
    },
};
